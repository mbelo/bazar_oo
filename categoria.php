<?php
require_once("./comum.php");
require_once("./usuario_autenticado.php");

use Mbelo\BazarOo\Categoria;

$categorias = Categoria::findAll();
?>
<html>
<head>
	<meta charset="UTF-8" />

	<title>Bazar Tem Tudo</title>

	<script>
	function confirmarExclusao(descricao) {
		return window.confirm('Deseja excluir a categoria ' + descricao + ' ?');
	}
	</script>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">
		<?php 
			if( isset($_SESSION["flash"])) {
				echo $_SESSION["flash"];
				unset($_SESSION["flash"]);
			}
		?>
		<table border="1">
			<thead>
				<tr>
					<th>Código</th>
					<th>Descrição</th>
					<th>Taxa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach( $categorias as $categoria) {
				?>
				<tr>
					<td><?= $categoria->getIdCategoria() ?></td>
					<td><?= $categoria->getDescricao() ?></td>
					<td><?= $categoria->getTaxa() ?></td>
					<td>
						<form method="post" action="excluir_categoria.php" 
							onsubmit="return confirmarExclusao('<?= $categoria->getDescricao() ?>');">
							<input type="hidden" name="idCategoria" value="<?= $categoria->getIdCategoria() ?>"/>
							<input type="submit" value="Excluir"/>
						</form>
					</td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>

		<a href="form_categoria.php">Nova Categoria</a>
	</div>

	<?php require_once("rodape.inc"); ?>

</body>
</html>
