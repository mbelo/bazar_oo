# BAZAR_OO

## PASSO 1: Verificar se existe uma instalação de PHP disponível no path

`php -v`

- Precisa exibir uma mensagem como essa:

```
PHP 8.1.2-1ubuntu2.11 (cli) (built: Feb 22 2023 22:56:18) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.2, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.2-1ubuntu2.11, Copyright (c), by Zend Technologies
```    

## Instalando o Composer

- Instalar o composer.phar na raiz do projeto (https://getcomposer.org/download/)

## MySQL docker container

```sh
docker run --rm --name bazar-oo -p 3306:3306 -e MYSQL_ROOT_PASSWORD=vertrigo -d mariadb:10.4.13
mysql --protocol=tcp -u root -p
```

## Preparar banco

- `scripts.sql` dispnível na raiz do projeto

## Rodar servidor local

`php -S localhost:8080`
