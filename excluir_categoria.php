<?php
require_once("./comum.php");
require_once("./usuario_autenticado.php");

use Mbelo\BazarOo\Categoria;

$id = filter_input(INPUT_POST,"idCategoria", FILTER_SANITIZE_NUMBER_INT);

$categoria = Categoria::findById( $id);

$ret = $categoria->excluir();
if( $ret) {

    $_SESSION["flash"] = "Categoria " . $categoria->getDescricao() . " excluída com sucesso";
} else {

    $_SESSION["flash"] = "Erro feito";
}
Header("Location: categoria.php");
