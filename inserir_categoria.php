<?php
require_once("./comum.php");
require_once("./usuario_autenticado.php");

use Mbelo\BazarOo\Categoria;

$descricao = filter_input(INPUT_POST,"descricao", FILTER_SANITIZE_STRING);
$taxa = filter_input( INPUT_POST, "taxa", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

$categoria = new Categoria( null, $descricao, $taxa);

$resultado = Categoria::add( $categoria);

if( $resultado ) {
	
	$_SESSION["flash"] = "Categoria " . $categoria->getDescricao() . " criada com sucesso";
}
else {

	$_SESSION["flash"] = "Erro fatal";
}
header("Location: categoria.php");
